
#include <xpl.cuh>
#include <TimingRecord.cuh>
using namespace xpl;

/**
*	Function takes 3D cube data and computes 3x3x3 sum in each point (minus centerpoint)
*
*	This function performs caching in shared memory for spatial locality
*/
void sharedMemory_cube_sum(const DeviceBuffer<float>& cube, 
							DeviceBuffer<float>& cubeSum)
{
	//
	// Define a gird to be exexuted (32x4x2 block covers cube.size values)
	auto grid = DeviceGrid(cube.size(), DeviceMapping(32,4,2));
	// Shared memory object
	// Stencil indicatest 1x1x1 extra neighboor cvalues
	xpl::Smem<float> smem(grid, BlockStencil(1,1,1) );

	auto smem_sum_kernel = xpl_device_lambda()
	{

		// Store block data + stencil in shared memor:
		smem.cache(cube);	
		float sum = 0.0f;
		// Loop over 27 values and calculate sum
		for(int z : {-1,0,-1})
		{
			for(int y : {-1,0,-1})
			{
				#pragma unroll
				for(int x : {-1,0,-1})
				{
					float val = smem(y+threadIdx.y, x+threadIdx.x, z+threadIdx.z);
					sum += val;
				}
			}
		}
		// Remove the center value:
		float myVal = smem(threadIdx.y, threadIdx.x, threadIdx.z);
		sum -= myVal;
	

		// Get global indices:
		int i = device::getY(); // same as threadIdx.y  + blockIdx.y*blockDim.y
		int j = device::getX(); // same as threadIdx.x  + blockIdx.x*blockDim.x
		int k = device::getZ();

		if(i < cube.rows() && j < cube.cols() && k < cube.depth())
		{
			// store final sum to global memory
			cubeSum(i,j,k) = sum;
		} // If in bounds
	};
	device::execute(grid, smem_sum_kernel);
	CUDA_ERROR_ASSERT();
}
/**
*	Function takes 3D cube data and computes 3x3x3 sum in each point (minus centerpoint)
*
*	This function performs caching in texture cache 
*/
void texture_cube_sum(const TextureBuffer<float>& cube, 
					DeviceBuffer<float>& cubeSum)
{
	auto texture_sum_kernel = xpl_device_lambda()
	{
		int i = device::getY(); //  ~ threadIdx.y  + blockIdx.y*blockDim.y
		int j = device::getX();
		int k = device::getZ();

		if(i < cube.rows() && j < cube.cols() && k < cube.depth())
		{
			float myVal = cube(i,j,k);
			float sum = 0.0f;
			for(int z : {-1,0,-1})
			{
				for(int y : {-1,0,-1})
				{
					for(int x : {-1,0,-1})
					{
						float val = cube(i+y, j+x, z+k);
						sum += val;
					}
				}
			}
			// Remove the center value:
			sum -= myVal;
			// store final sum to global memory
			cubeSum(i,j,k) = sum;
		} // If in bounds
	};
	device::execute(cube.size(), texture_sum_kernel);
	CUDA_ERROR_ASSERT();
}
/**
*	Function takes 3D cube data and computes 3x3x3 sum in each point (minus centerpoint)
*	This naive function relies on direct global memory reads and L2/L1 caching only
*/
void naive_cube_sum(const DeviceBuffer<float>& cube, 
					DeviceBuffer<float>& cubeSum)
{
	auto naiveKernel = xpl_device_lambda()
	{
		int i = device::getY(); //  ~ threadIdx.y  + blockIdx.y*blockDim.y
		int j = device::getX();
		int k = device::getZ();


		if(i < cube.rows() && j < cube.cols() && k < cube.depth())
		{

			float myVal = cube(i,j,k);
			float sum = 0.0f;
			for(int z : {-1,0,-1})
			{
				for(int y : {-1,0,-1})
				{
					for(int x : {-1,0,-1})
					{

						// Boundary checking:
						bool outOfBounds =  (x+j) < 0 ||  (x+j) >= cube.cols() ||
											(y+i) < 0 ||  (y+i) >= cube.rows() ||
											(z+k) < 0 ||  (z+k) >= cube.depth();

						float val = outOfBounds ? 0.0f : cube(i+y, j+x, z+k);
						sum += val;
					}
				}
			}
			// Remove the center value:
			sum -= myVal;
			// store final sum to global memory
			cubeSum(i,j,k) = sum;
		} // If in bounds
	};
	device::execute(cube.size(), naiveKernel);
	CUDA_ERROR_ASSERT();
}


int main(int argc, char* argv[])
{


	HostBuffer<float> h_cube(300,300,300);

	// Create some random input data:
	h_cube.for_each([&](float& val)
	{
		val = float(rand())/float(RAND_MAX);
	});

	DeviceBuffer<float> cube(h_cube.size());
	DeviceBuffer<float> cubeSum(cube.size());
	TextureBuffer<float> tex_cube(cube.size());
	// Memcopy to GPU buffer
	cube = h_cube;
	//
	// setup some timers
	const uint NbIters = 80;
	utils::TimingRecord record("cubeSum");
	record.setupEventTimer("naive_cube_sum", NbIters); // NOTE preallocating 100 timers
	record.setupEventTimer("texture_cube_sum", NbIters); // NOTE preallocating 100 timers
	record.setupEventTimer("sharedMemory_cube_sum", NbIters); // NOTE preallocating 100 timers

	//
	// Benchmark naive version:
	for(uint t = 0; t < NbIters; t++)
	{
		auto timer = record.get("naive_cube_sum")->newTimer();
		timer->start();
		naive_cube_sum(cube,cubeSum);
		timer->stop();
	}
	//
	// Texture version:
	for(uint t = 0; t < NbIters; t++)
	{
		auto timer = record.get("texture_cube_sum")->newTimer();
		timer->start();
		texture_cube_sum(tex_cube,cubeSum);
		timer->stop();
	}
	//
	// Shared memory version
	for(uint t = 0; t < NbIters; t++)
	{
		auto timer = record.get("sharedMemory_cube_sum")->newTimer();
		timer->start();
		sharedMemory_cube_sum(cube,cubeSum);
		timer->stop();
	}
	// Print timing resutls:
	std::cout << record;
	return 0;
}